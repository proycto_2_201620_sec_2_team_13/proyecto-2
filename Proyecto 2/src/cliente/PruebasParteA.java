package cliente;

import java.io.BufferedWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.InputMismatchException;
import java.util.Scanner;

import json.ProcessJSONCasualties;
import json.ProcessJSONFlow;
import taller.estructuras.*;
import taller.mundo.*;



public class PruebasParteA {
	
	BufferedWriter escritor;
	Scanner lector;
	Ciudad[] datos;
	ProcessJSONCasualties carga;
	MaxHeap heap;
	TablaHashChain<String,TablaHashLineal<Integer,Integer>> tablaChain;
	TablaHashLineal<Integer, Integer> tablaLineal;
	
	//TODO: Declarar objetos de la parte A
	
	public PruebasParteA(BufferedWriter escritor, Scanner lector) {
		this.escritor = escritor;
		this.lector = lector;
	}
	
	public void pruebas() {
		int opcion = -1;
		
		//TODO: Inicializar objetos de la parte A
		
		long tiempoDeCarga = System.nanoTime();
		tablaChain = new TablaHashChain<>();
		//TODO: Cargar informacion de la parte A
		carga = new ProcessJSONCasualties("./data/road-casualties-severity-borough.json");
		datos = carga.getDatos();
		heap = new MaxHeap(datos.length);
		tiempoDeCarga = System.nanoTime() - tiempoDeCarga;
		
		while (opcion != 0) {
			try {
				escritor.write("---------------Pruebas Proyecto ---------------\n");
				escritor.write("Informacion cargada en: " + tiempoDeCarga + " nanosegundos\n");
				escritor.write("Reportes:\n");
				escritor.write("1: Imprimir la informaci�n de cada local_authority de mayor a menor prioridad. \n");
				escritor.write("2: Responder consultas sobre el n�mero de accidentes en un a�o particular. \n");
				escritor.write("3: Conocer los accidentes, flujo de carros y flujo de todos los veh�culos, de una local_authority (dado su nombre), en un a�o espec�fico \n");
				escritor.write("0: Volver\n");
				escritor.write("------------------------------------------------\n");
				escritor.flush();
				opcion = lector.nextInt();
				
				switch(opcion) {
				case 1: a1(); break;
				case 2: a2(); break;
				case 3: a3(); break;
				
				}
			}
			catch (IOException ioe) {
				ioe.printStackTrace();
			}
			catch (NumberFormatException nfe) {
				try {
					escritor.write("No ingreso el periodo de tiempo correctamente\n");
					escritor.write("Ingrese cualquier letra y enter para continuar\n");
					escritor.flush();
					lector.nextLine();
					lector.nextLine();
				}
				catch (IOException ioe) {
					ioe.printStackTrace();
				}
			}
			catch (InputMismatchException ime) {
				try {
					escritor.write("No ingreso un numeral\n");
					escritor.write("Ingrese cualquier letra y enter para continuar\n");
					escritor.flush();
					lector.nextLine();
					lector.nextLine();
				}
				catch (IOException ioe) {
					ioe.printStackTrace();
				}
			}
		}
	}
	
	private void a1() throws IOException{
		long tiempo = System.nanoTime();
		//TODO: Genere la prioridad de cada local_authority usando la media ponderada de los accidentes de todos los a�os.
		//Recuerde que los pesos por a�o corresponden a un porcentaje y que estos pesos se encuentran en el archivo road-casualties-severity-borough-years-weight.json.
		System.out.println("------------------------------------------------");
		//TODO: Defina una cola de prioridad orientada a mayor a partir de los datos que se encuentran en el archivo road-casualties-severity-borough.json.
		for (int i=0; i<datos.length; i++)
		{
			heap.add(datos[i]);
		}
		for (int i=0; i<heap.size(); i++)
		{
			Ciudad max = heap.poll();
			DecimalFormat df = new DecimalFormat("#,###.##########");
			System.out.println(max.getAutoridad() + ", " + df.format(max.getPrioridad()));
		}
		
		//TODO: Obtener e imprimir la informaci�n de cada local_authority de mayor a menor prioridad, incluyendo el valor de prioridad, ejemplo: <local-authority_x>, <prioridad_local_authority_x>.
		//RECUERDE: Debe hacer uso del algoritmo HeapSort
		tiempo = System.nanoTime() - tiempo;
		escritor.write("Duracion: " + tiempo + " nanosegundos\n");
		escritor.write("Ingrese cualquier letra y Enter para continuar\n");
		escritor.flush();
		lector.next();
	}
	
	private void a2() throws IOException{
		//TODO: Defina una tabla de hash aplicando la t�cnica "Separate Chaining" para el indexamiento por c�digo de local_authority
		//y utilizando como llave el c�digo de las local_authorities 
		
		//RECUERDE: El valor asociado a cada llave es el conjunto de accidentes detallado por a�o para la Local Authority
		//y estos valores se indexar�n nuevamente en una segunda tabla de hash utilizando una implementaci�n de "Linear Probing"
		
		//TODO: Defina una segunda tabla de hash utilizando una implementaci�n de "Linear Probing".
		//En esta segunda tabla la llave es el a�o y el valor el n�mero de accidentes de dicho a�o.
		
		//RECUERDE: La segunda tabla de hash esta almacenada dentro de la primera tabla de hash: Estructura de datos doble hash 
		
		//TODO: Solicitar al usuario la informaci�n requerida para el desarrollo del literal: c�digo de una local_authority y a�o de consulta
		//RECUERDE: El objeto del literal es responder consultas sobre el n�mero de accidentes en un a�o particular de una local_authority dado su c�digo.
		//En ese orden de ideas debe solicitar al usuario un a�o y el c�digo de una local_authority
		System.out.println("------------------------------------------------");
		for (int i=0; i<datos.length; i++)
		{
			Valor[] valor = datos[i].getValor();
			tablaLineal = new TablaHashLineal<>();
			for (int j=0; j<valor.length; j++)
			{
				tablaLineal.put(valor[j].getAño(), valor[j].getNumero());
			}
			tablaChain.put(datos[i].getCodigo(), tablaLineal);
		}
		
		//TODO: Obtener e imprimir la informaci�n sobre el n�mero de accidentes en el a�o de interes.
		
		System.out.println("Ingrese el código de la local authority");
		String codigo = lector.next();
		System.out.println("Ingrese el año a consultar");
		String anio = lector.next();
		
		long tiempo = System.nanoTime();
		TablaHashLineal<Integer, Integer> datoUsuario = tablaChain.get(codigo);
		System.out.println("Nº de accidentes: " + datoUsuario.get(Integer.parseInt(anio)) +"\n");
		tiempo = System.nanoTime() - tiempo;
		escritor.write("Duracion: " + tiempo + " nanosegundos\n");
		escritor.write("Ingrese cualquier letra y Enter para continuar\n");
		escritor.flush();
		lector.next();		
	}
	
	private void a3() throws IOException{
		//TODO: Construya un �rbol binario balanceado ordenado por el nombre de las local_authority (llave).
		//En cada nodo se debe tener asociado el c�digo de la local_autority. 
		
		//TODO: Construya un segundo �rbol binario balanceado ordenado por el c�digo de las local_authority (llave).
	    //En cada nodo se debe tener dos tablas de hash:
		
		//TABLA 1: Esta tabla debe contener la informaci�n de "traffic flow cars". Llave: a�o de consulta
		//TABLA 2: Esta tabla debe contener la informaci�n de "traffic flow all vehicles". Llave: a�o de consulta 
		
		//RECUERDE: La implementaci�n de la primera tabla se debe hacer mediante "Separate Chaining" y la segunda mediante "Linear Probing"
		
		//TODO: Solicitar al usuario la informaci�n requerida para el desarrollo del literal: nombre de una local_authority y a�o de consulta
		//RECUERDE: El objeto del literal es darle a conocer al usuario los accidentes, flujo de carros y flujo de todos los veh�culos, de una local_authority dado su nombre, en un a�o espec�fico.
		//En ese orden de ideas debe solicitar al usuario un a�o y el nombre de una local_authority
		ArbolRossoNero<String, String> arbol1 = new ArbolRossoNero<>();
		ArbolRossoNero<String, Nodo2Valores<TablaHashChain<Integer, Integer>,TablaHashLineal<Integer, Integer>>> arbol2 = new ArbolRossoNero<>();
		for (int i=0; i<datos.length; i++)
		{
			Valor[] valor = datos[i].getValor();
			tablaLineal = new TablaHashLineal<>();
			for (int j=0; j<valor.length; j++)
			{
				tablaLineal.put(valor[j].getAño(), valor[j].getNumero());
			}
			tablaChain.put(datos[i].getCodigo(), tablaLineal);
			arbol1.put(datos[i].getAutoridad(), datos[i].getCodigo());
		}
		
		ProcessJSONFlow cars = new ProcessJSONFlow("./data/traffic-flow-borough-cars.json");
		ProcessJSONFlow all = new ProcessJSONFlow("./data/traffic-flow-borough-all.json");
		Ciudad [] datosCars = cars.getDatos(); 
		Ciudad[] datosAll = all.getDatos();
		
		for (int i=0; i<datosCars.length; i++)
		{
			Valor [] valor1 = datosCars[i].getValor(); 
			Valor [] valor2 = datosAll[i].getValor(); 
			TablaHashChain<Integer, Integer> tabla1 = new TablaHashChain<>();
			TablaHashLineal<Integer,Integer> tabla2 = new TablaHashLineal<>();
			for (int j=0; j< valor1.length; j++)
			{
				tabla1.put(valor1[j].getAño(), valor1[j].getNumero());
				tabla2.put(valor1[j].getAño(), valor2[j].getNumero());
			}
			arbol2.put(datosAll[i].getCodigo(), new Nodo2Valores<TablaHashChain<Integer,Integer>, TablaHashLineal<Integer,Integer>>(tabla1, tabla2));
		}
		System.out.println("------------------------------------------------");
		System.out.println("Ingrese el nombre de la local authority");
		String nombre = lector.next();
		System.out.println();
		System.out.println("Ingrese el año a consultar");
		String anio = lector.next();
		
		long tiempo = System.nanoTime();
		//TODO: Obtener e imprimir la informaci�n sobre los accidentes, flujo de carros y flujo de todos los veh�culos dado el nombre del local_authority de interes.
		String cod = arbol1.get(nombre);
		System.out.println(cod);
		TablaHashLineal<Integer, Integer> datoUsuario = tablaChain.get(cod);
		Nodo2Valores<TablaHashChain<Integer, Integer>, TablaHashLineal<Integer,Integer>> resultado = arbol2.get(cod);
		TablaHashChain<Integer, Integer> tabla1 = resultado.getValue1();
		TablaHashLineal<Integer,Integer> tabla2 = resultado.getValue2();
		String accidentes = "" + datoUsuario.get(Integer.parseInt(anio));
		String carros = "" + tabla1.get(Integer.parseInt(anio));
		String total = "" + tabla2.get(Integer.parseInt(anio));
		if (datoUsuario.get(Integer.parseInt(anio)) == null) accidentes = "" + 0;
		if (tabla1.get(Integer.parseInt(anio)) == null) carros = "" + 0;
		if (tabla2.get(Integer.parseInt(anio)) == null) total = "" + 0;
		System.out.println("Nº de accidentes: " + accidentes);
		System.out.println("Nº de flujo de carros: " + carros);
		System.out.println("Nº de flujo en total: "+ total);
		tiempo = System.nanoTime() - tiempo;
		escritor.write("Duracion: " + tiempo + " nanosegundos\n");
		escritor.write("Ingrese cualquier letra y Enter para continuar\n");
		escritor.flush();
		lector.next();		
	}
}
	
