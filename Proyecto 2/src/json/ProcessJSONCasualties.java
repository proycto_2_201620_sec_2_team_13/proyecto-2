package json;

import java.io.FileNotFoundException;
import java.io.FileReader;

import taller.mundo.Ciudad;
import taller.mundo.Valor;

import com.google.gson.JsonArray;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;


/**
 * Clase copiada del tutorial de Piazza
 */
public class ProcessJSONCasualties {
	private static Ciudad[] lista;

	public ProcessJSONCasualties (String ruta)
	{
		lista = new Ciudad[49];
		mostrarObjetos(ruta);
	}

	public static void mostrarObjetos(String ruta)
	{
		JsonParser parser = new JsonParser();

		try {
			JsonArray arr= (JsonArray) parser.parse(new FileReader(ruta));
			JsonArray arr2= (JsonArray) parser.parse(new FileReader("./data/road-casualties-severity-borough-years-weight.json"));
			
			for (int i = 0; i < arr.size(); i++)
			{
				JsonObject obj= (JsonObject) arr.get(i);

				String code= obj.get("Code").getAsString();
				String localAuth=obj.get("Local-Authority").getAsString();
				lista[i] = new Ciudad (code, localAuth, 11);
				JsonObject obj2 = (JsonObject) arr2.get(0);
				for (int j = 2004; j <= 2014; j++)
				{	
					int numero = obj.get(""+j).getAsInt();
					double peso = obj2.get(""+j).getAsDouble();
					lista[i].setValor(j, numero, peso);
				}
				lista[i].calculatePrioridad();
			}
			
		} catch (JsonIOException | JsonSyntaxException | FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public Ciudad[] getDatos (){
		return lista;
	}

}
