package taller.mundo;

public class Valor {

	private int año;
	private int numero;
	private double porcentaje;
	
	public Valor(int año, int numero, double porcentaje) {
		super();
		this.año = año;
		this.numero = numero;
		this.porcentaje = porcentaje;
	}
	public int getAño() {
		return año;
	}
	public void setAño(int año) {
		this.año = año;
	}
	public int getNumero() {
		return numero;
	}
	public void setNumero(int numero) {
		this.numero = numero;
	}
	public double getPorcentaje() {
		return porcentaje;
	}
	public void setPorcentaje(double porcentaje) {
		this.porcentaje = porcentaje;
	}
	
	
}
