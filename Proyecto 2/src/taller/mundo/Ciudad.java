package taller.mundo;

public class Ciudad implements Comparable<Ciudad>{
	
	private Valor[] valor;
	private String codigo;
	private String autoridad;
	private double prioridad;
	
	public Ciudad(String codigo, String autoridad, int tamañoDatos) {
		this.autoridad = autoridad;
		this.codigo = codigo;
		valor = new Valor[tamañoDatos];
	}

	
	
	public Valor[] getValor() {
		return valor;
	}



	public void setValor(int año, int numero, double porcentaje) {
		boolean encontro = false;
		for (int i=0; i< valor.length && !encontro; i++){
			if(valor[i] == null){
				valor[i] = new Valor(año, numero, porcentaje);
				encontro = true;
			}
		}
	}



	public double getPrioridad() {
		
		
		return prioridad;
	}



	public void calculatePrioridad() {
		for (int i=0; i< valor.length; i++){
			prioridad += valor[i].getNumero()*valor[i].getPorcentaje();
		}
	}



	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getAutoridad() {
		return autoridad;
	}

	public void setAutoridad(String autoridad) {
		this.autoridad = autoridad;
	}

	/**
	 * Método para comparar la prioridad entre ciudades
	 * Retorna -1 cuando a tiene menos prioridad que b
	 * Retorna 1 cuando a tiene mas prioridad que b
	 * @return valor. Valor de comparacion de a.compareTO(b) 
	 */
	@Override
	public int compareTo(Ciudad o) {
		// TODO Auto-generated method stub
		if ( this.getPrioridad() < o.getPrioridad())
			return -1;
		else if (this.getPrioridad() > o.getPrioridad())
			return 1;
		else
			return 0;
	}
	
	

}
