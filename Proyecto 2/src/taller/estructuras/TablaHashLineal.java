package taller.estructuras;

import java.util.ArrayList;


public class TablaHashLineal<K extends Comparable<K> ,V> {

	//TODO Una enumeración que representa los tipos de colisiones que se pueden manejar

	/**
	 * Factor de carga actual de la tabla (porcentaje utilizado).
	 */
	private float factorCarga;

	/**
	 * Factor de carga maximo que soporta la tabla.
	 */
	private float factorCargaMax;

	/**
	 * Estructura que soporta la tabla.
	 */
	//TODO: Agruegue la estructura que va a soportar la tabla.
	private NodoHash<K, V>[] tabla;

	/**
	 * La cuenta de elementos actuales.
	 */
	private int count;

	/**
	 * La capacidad actual de la tabla. Tamaño del arreglo fijo.
	 */
	private int capacidad;
	

	//Constructores

	@SuppressWarnings("unchecked")
	public TablaHashLineal(){
		//TODO: Inicialice la tabla con los valores que considere prudentes para una ejecución normal
		this.capacidad = 100;
		this.tabla = new NodoHash[capacidad];
		this.factorCargaMax = (float) 1.0;
	}

	@SuppressWarnings("unchecked")
	public TablaHashLineal(int capacidad, float factorCargaMax) {
		//TODO: Inicialice la tabla con los valores dados por parametro
		this.capacidad = capacidad;
		this.factorCargaMax = factorCargaMax;
		this.tabla = new NodoHash[capacidad];
	}

	public void put(K llave, V valor){
		//TODO: Guarde el objeto valor dado por parametro el cual tiene la llave,
		//tenga en cuenta que puede o no puede haber colisiones
		int pos = hash(llave);
		if(tabla[pos] == null)
			tabla[pos] = new NodoHash<K, V>(llave, valor);
		else{
			while (tabla[pos] != null)
			{
				pos++;
				if (pos == capacidad)
					pos = 0;
			}
			if (tabla[pos] == null)
				tabla[pos] = new NodoHash<K, V>(llave, valor);
			else
				calcularFactorCarga();
		}
		count++;
		
	}

	public V get(K llave){
		//TODO: Busque y retorne el objeto cuya llave es la dada por parametro. Tenga en cuenta
		// colisiones
		int pos = hash(llave);
		if(tabla[pos] == null)
			return null;
		else{
			
			if (tabla[pos].getLlave().equals(llave))
				return tabla[pos].getValor();
			while (tabla[pos] != null && !tabla[pos].getLlave().equals(llave))
				{
					pos++;
					if (pos == capacidad) pos = 0;	
				}
				if (tabla[pos].getLlave().equals(llave))
					return tabla[pos].getValor();
				else
					return null;
		}
	}

	
	//Hash
	private int hash(K llave)
	{
		//TODO: Escriba una función de Hash, recuerde tener en cuenta la complejidad de ésta así como las colisiones.
		int valor = 0;
		for (int i = 0; i < llave.toString().length(); i++)
		{
			valor += llave.toString().codePointAt(i);
		}
		return valor % capacidad;
	}
	
		//TODO: Permita que la tabla sea dinamica

	private void calcularFactorCarga()
	{
			NodoHash<K, V>[] vieja = tabla;
			capacidad += capacidad;
			this.tabla = new NodoHash[capacidad];
			for (int i=0; i<vieja.length;i++)
			{                     
				if(vieja[i] != null){
					NodoHash<K, V> actual = vieja[i];
					V valor = actual.getValor();
					put(actual.getLlave(), valor);
				}
			}
	}

}