package taller.estructuras;

public class NodoArbol<K extends Comparable<K>,V> {
	public K key;
	public V value;
	public NodoArbol<K,V> left;
	public NodoArbol<K,V> right;
	public int N;
	public boolean color;
	
	public NodoArbol(K key, V value, int n, boolean color) {
		
		this.key = key;
		this.value = value;
		N = n;
		this.color = color;
		
	}
	public K getKey() {
		return key;
	}
	public void setKey(K key) {
		this.key = key;
	}
	public V getValue() {
		return value;
	}
	public void setValue(V value) {
		this.value = value;
	}
	public NodoArbol<K, V> getLeft() {
		return left;
	}
	public void setLeft(NodoArbol<K, V> left) {
		this.left = left;
	}
	public NodoArbol<K, V> getRight() {
		return right;
	}
	public void setRight(NodoArbol<K, V> right) {
		this.right = right;
	}
	public int getN() {
		return N;
	}
	public void setN(int n) {
		N = n;
	}
	public boolean isColorRed() {
		return color;
	}
	public void setColor(boolean color) {
		this.color = color;
	}
	
}
