package taller.estructuras;

import taller.mundo.Ciudad;

public class MaxHeap implements IHeap<Ciudad>{

	private Ciudad[] pq; // heap-ordered complete binary tree

	private int N = 0; // in pq[1..N] with pq[0] unused

	public MaxHeap(int maxN)
	{ pq = (Ciudad[]) new Ciudad[maxN+1]; }

	public boolean isEmpty()
	{ return N == 0; }

	public int size()
	{ return N; }

	
	@Override
	public void add(Ciudad elemento) {
		// TODO Auto-generated method stub
		pq[++N] = elemento;
		siftUp();
	}

	@Override
	public Ciudad peek() {
		// TODO Auto-generated method stub
		return pq[1];
	}

	@Override
	public Ciudad poll() {
		// TODO Auto-generated method stub
		Ciudad max = pq[1]; // Retrieve max key from top.
		pq[1] = null;
		exch(1, N--); // Exchange with last item.
		siftDown(); // Restore heap property.
		return max;
	}


	@Override
	public void siftUp() {
		// TODO Auto-generated method stub
		int n = N;
		while (n > 1 && less(n/2,n))
		{
			exch(n/2, n);
			n = n/2;
		} 
	}

	@Override
	public void siftDown() {
		// TODO Auto-generated method stub
		int k = 1;
		while (2*k <= N)
		{
			int j = 2*k;
			 if (j < N && less(j, j+1)) j++;
			 if (!less(k, j)) break;
			 exch(k, j);
			 k = j;
		} 
	}
	
	private boolean less(int i, int j)
	{ return pq[i].compareTo(pq[j]) < 0; }
	private void exch(int i, int j)
	{ Ciudad t = pq[i]; pq[i] = pq[j]; pq[j] = t; }


}
