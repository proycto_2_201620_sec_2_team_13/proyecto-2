package taller.estructuras;

/**
 * Clase adquirida de Algorithms Fourth Edition
 * Copyright © 2000–2016 Robert Sedgewick and Kevin Wayne.
 * http://algs4.cs.princeton.edu/33balanced/RedBlackBST.java.html
 */
public class ArbolRossoNero<K extends Comparable<K>,V> {
	
	private NodoArbol<K, V> root;
	
	
	public ArbolRossoNero(){
		root = null;
	}

	 private boolean isRed(NodoArbol<K, V> x) {
	        if (x == null) return false;
	        return x.color == true;
	    }
	 
	 private int size(NodoArbol<K, V> x) {
	        if (x == null) return 0;
	        return x.N;
	    } 
	 public int size() {
	        return size(root);
	    }
	 public boolean isEmpty() {
	        return root == null;
	    }
	 
	 public V get(K key) {
	        if (key == null) throw new NullPointerException("argument to get() is null");
	        return get(root, key);
	    }
	 
	 private V get(NodoArbol<K, V> x, K key) {
	        while (x != null) {
	            int cmp = key.compareTo(x.key);
	            if      (cmp < 0) x = x.left;
	            else if (cmp > 0) x = x.right;
	            else              return x.value;
	        }
	        return null;
	    }
	 
	 public boolean contains(K key) {
	        return get(key) != null;
	    }
	 
	 public void put(K key, V val) {

	        root = put(root, key, val);
	        root.color = false;
	        // assert check();
	    }

	    // insert the key-value pair in the subtree rooted at h
	    private NodoArbol<K,V> put(NodoArbol<K, V> h, K key, V val) { 
	        if (h == null) return new NodoArbol<K,V>(key, val, 1, true);

	        int cmp = key.compareTo(h.key);
	        if      (cmp < 0) h.left  = put(h.left,  key, val); 
	        else if (cmp > 0) h.right = put(h.right, key, val); 
	        else              h.value   = val;

	        // fix-up any right-leaning links
	        if (isRed(h.right) && !isRed(h.left))      h = rotateLeft(h);
	        if (isRed(h.left)  &&  isRed(h.left.left)) h = rotateRight(h);
	        if (isRed(h.left)  &&  isRed(h.right))     flipColors(h);
	        h.N = size(h.left) + size(h.right) + 1;

	        return h;
	    }
	    
	    private NodoArbol<K,V> rotateRight(NodoArbol<K,V> h) {
	        // assert (h != null) && isRed(h.left);
	        NodoArbol<K,V> x = h.left;
	        h.left = x.right;
	        x.right = h;
	        x.color = x.right.color;
	        x.right.color = true;
	        x.N = h.N;
	        h.N = size(h.left) + size(h.right) + 1;
	        return x;
	    }

	    // make a right-leaning link lean to the left
	    private NodoArbol<K,V> rotateLeft(NodoArbol<K,V> h) {
	        // assert (h != null) && isRed(h.right);
	    	NodoArbol<K,V> x = h.right;
	        h.right = x.left;
	        x.left = h;
	        x.color = x.left.color;
	        x.left.color = true;
	        x.N = h.N;
	        h.N = size(h.left) + size(h.right) + 1;
	        return x;
	    }

	    // flip the colors of a node and its two children
	    private void flipColors(NodoArbol<K,V> h) {
	        // h must have opposite color of its two children
	        // assert (h != null) && (h.left != null) && (h.right != null);
	        // assert (!isRed(h) &&  isRed(h.left) &&  isRed(h.right))
	        //    || (isRed(h)  && !isRed(h.left) && !isRed(h.right));
	        h.color = !h.color;
	        h.left.color = !h.left.color;
	        h.right.color = !h.right.color;
	    }

}
